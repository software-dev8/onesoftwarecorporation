/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.pattraporn.onesoftwarecorporation.dao;

import com.pattraporn.onesoftwarecorporation.helper.DatabaseHelper;
import com.pattraporn.onesoftwarecorporation.model.Employee;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author User
 */
public class EmployeeDao implements Dao<Employee> {

    @Override
    public Employee get(int id) {
   Employee employee = null;
        String sql = "SELECT * FROM Employee WHERE EM_ID=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                employee = Employee.fromRS(rs);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return  employee;
    }

    
    public List<Employee> getAll(String name) {
    
        ArrayList<Employee> list = new ArrayList();
        String sql = "SELECT * FROM Employee";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                Employee employee = Employee.fromRS(rs);
                list.add(employee);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    } 

    @Override
    public Employee save(Employee obj) {
   String sql = "INSERT INTO Employee ( EM_ID, EM_Firstname, EM_Surname, EM_Position,EM_IP_Address,EM_Phone,EM_Salary)"
                + "VALUES(?, ?, ?, ?, ?, ?, ?)";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getId());
            stmt.setString(2, obj.getName());
            stmt.setString(3, obj.getSurname());
            stmt.setString(4, obj.getPosition());
            stmt.setInt(5, obj.getIp_address());
            stmt.setInt(6, obj.getPhone());
            stmt.setInt(7, obj.getSalary());
//            System.out.println(stmt);
            stmt.executeUpdate();
            int id = DatabaseHelper.getInsertedId(stmt);
            obj.setId(id);
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
        return obj;
    }

    @Override
    public Employee update(Employee obj) {
    String sql = "UPDATE Employee"
                + " SET EM_ID = ?, EM_Firstname = ?,EM_Surname = ?, EM_Position = ?, EM_IP_Address = ?, EM_Phone = ?, EM_Salary = ?"
                + " WHERE EM_ID = ?";
    
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
                    stmt.setInt(1, obj.getId());
            stmt.setString(2, obj.getName());
            stmt.setString(3, obj.getSurname());
            stmt.setString(4, obj.getPosition());
            stmt.setInt(5, obj.getIp_address());
            stmt.setInt(6, obj.getPhone());
            stmt.setInt(7, obj.getSalary());
//            System.out.println(stmt);
            int ret = stmt.executeUpdate();
            System.out.println(ret);
            return obj;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
    }

    @Override
    public int delete(Employee obj) {
    String sql = "DELETE FROM Employee WHERE EM_ID=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getId());
            int ret = stmt.executeUpdate();
            return ret;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return -1;  
    }

    @Override
    public List<Employee> getAll(String where, String order) {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    @Override
    public List<Employee> getAll() {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

   
    
}
