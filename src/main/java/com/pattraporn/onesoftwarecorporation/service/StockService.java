/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pattraporn.onesoftwarecorporation.service;

import com.pattraporn.onesoftwarecorporation.dao.StockDao;
import com.pattraporn.onesoftwarecorporation.dao.UserDao;
import com.pattraporn.onesoftwarecorporation.model.Stock;
import com.pattraporn.onesoftwarecorporation.model.User;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Pattrapon N
 */
public class StockService {
      private static ArrayList<Stock> Stocklist = new ArrayList<>();

   

    public static boolean addStock(Stock stock) {
       Stocklist.add(stock);
        return true;
    }

    public static boolean addStock(String name, String type) {
        Stocklist.add(new Stock( name,type));
        return true;
    }
   
    
  public List<Stock>getStock(){
      StockDao stockDao = new StockDao();
      return stockDao.getAll("Stock_name asc");
  }

    public Stock addNew(Stock edtedStock) {
        StockDao stockDao = new StockDao();
         return stockDao.save(edtedStock);
    }

    public Stock update(Stock edtedStock) {
          StockDao stockDao = new StockDao();
         return  stockDao.update(edtedStock);
    }

    public int delete(Stock edtedStock) {
       StockDao stockDao = new StockDao();
         return stockDao.delete(edtedStock);
    }
}
