/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.anurak.onesoftwarecorporation.report;

import com.pattraporn.onesoftwarecorporation.dao.EmployeeDao;
import com.pattraporn.onesoftwarecorporation.dao.OrdersDao;
import com.pattraporn.onesoftwarecorporation.helper.DatabaseHelper;
import com.pattraporn.onesoftwarecorporation.model.Employee;
import com.pattraporn.onesoftwarecorporation.model.Orders;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author anu01
 */
public class orderTime {
    /**
     Amounts of Order in each Employee
     */
    
    public int getCount(int ID) {
        int count = 0;
        String sql = "SELECT COUNT(*) FROM Order WHERE EM_ID = " + ID;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                com.pattraporn.onesoftwarecorporation.model.Employee item = com.pattraporn.onesoftwarecorporation.model.Employee.fromRS(rs);
                count = rs.getInt(0);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return count;
    }

    private  OrdersDao odao = new OrdersDao();
    private  EmployeeDao empdao = new EmployeeDao();
    private List<Orders> orderList;
    private List<Employee> employeeList;
    
    orderTime() {
        employeeList = empdao.getAll();
        for (Employee i : employeeList){
            System.out.printf("%s = %d\n", i.getName() , getCount(i.getId()));
        }
    }
    
       public static void main(String[] args) {
//           OrdersDao odao = new OrdersDao();
//           System.out.println(odao.getAll());
    }
}
