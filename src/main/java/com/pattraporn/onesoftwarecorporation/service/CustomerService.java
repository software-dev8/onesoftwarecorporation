/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pattraporn.onesoftwarecorporation.service;

import com.pattraporn.onesoftwarecorporation.dao.CustomerDao;
import com.pattraporn.onesoftwarecorporation.model.Customer;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author roman
 */
public class CustomerService {
         private static ArrayList<Customer> Customerlist = new ArrayList<>();

   

    public static boolean addCustomer(Customer customer) {
       Customerlist.add(customer);
        return true;
    }

    public static boolean addCustomer(String name, String status) {
        Customerlist.add(new Customer( name,status));
        return true;
    }
   
    
  public List<Customer>getCustomer(){
      CustomerDao customerDao = new CustomerDao();
      return customerDao.getAll("Customer_name asc");
  }

    public Customer addNew(Customer edtedCustomer) {
        CustomerDao customerDao = new CustomerDao();
         return (Customer) customerDao.save(edtedCustomer);
    }

    public Customer update(Customer edtedCustomer) {
          CustomerDao customerDao = new CustomerDao();
         return  (Customer) customerDao.update(edtedCustomer);
    }

    public int delete(Customer edtedCustomer) {
       CustomerDao customerDao = new CustomerDao();
         return customerDao.delete(edtedCustomer);
    }
}
