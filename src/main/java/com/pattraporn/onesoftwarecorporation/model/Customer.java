/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pattraporn.onesoftwarecorporation.model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author roman
 */
public class Customer {

    private int id;
    private String name;   
    private int phonenumber;
    private String address;

 

    public Customer(String name, String address) {
        this.name = name;
        this.address = address;
    }

    public Customer(int id, String name, int phonenumber, String address) {
        this.id = id;
        this.name = name;
        this.phonenumber = phonenumber;
        this.address = address;

    }

    public Customer(String name, int phonenumber,String address) {
        this.id = -1;
        this.name = name;
        this.phonenumber = phonenumber;
        this.address = address;

    }

    public Customer() {
        this.id = -1;
        this.name = "";
        this.phonenumber = phonenumber;
        this.address = address;


    }

    @Override
    public String toString() {
        return "Customer{" + "id=" + id + ", name=" + name + ", phonenumber=" + phonenumber + ", address=" + address +'}';
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getPhonenumber() {
        return phonenumber;
    }

    public void setPhonenumber(int phonenumber) {
        this.phonenumber = phonenumber;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }


    public static Customer fromRS(ResultSet rs) throws SQLException {
        Customer cus = new Customer();
        try {
            cus.setId(rs.getInt("Cus_ID"));
            cus.setName(rs.getString("Cus_Name"));
            cus.setPhonenumber(rs.getInt("Cus_PhoneNumber"));
            cus.setAddress(rs.getString("Cus_Address"));
        } catch (SQLException ex) {

            Logger.getLogger(Stock.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return cus;
    }
}
