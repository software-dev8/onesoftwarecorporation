--
-- File generated with SQLiteStudio v3.3.3 on Tue Nov 1 00:33:50 2022
--
-- Text encoding used: UTF-8
--
PRAGMA foreign_keys = off;
BEGIN TRANSACTION;

-- Table: Bill
DROP TABLE IF EXISTS Bill;

CREATE TABLE Bill (
    Bil_ID     INTEGER PRIMARY KEY,
    Bill_price INTEGER,
    Bill_total INTEGER,
    Bill_Date  DATE
);


-- Table: Category
DROP TABLE IF EXISTS Category;

CREATE TABLE Category (
    category_id   INTEGER PRIMARY KEY,
    category_name TEXT
);


-- Table: CheckIn
DROP TABLE IF EXISTS CheckIn;

CREATE TABLE CheckIn (
    CI_ID   INTEGER PRIMARY KEY,
    CI_TIME TIME,
    CI_DATE DATE,
    EM_ID   INTEGER REFERENCES Employee (EM_ID) 
);


-- Table: CheckOut
DROP TABLE IF EXISTS CheckOut;

CREATE TABLE CheckOut (
    CO_ID   INTEGER PRIMARY KEY,
    CO_TIME TIME,
    CO_DATE DATE,
    EM_ID   INTEGER REFERENCES Employee (EM_ID) 
);


-- Table: Customer
DROP TABLE IF EXISTS Customer;

CREATE TABLE Customer (
    Cus_ID          INTEGER    PRIMARY KEY,
    Cus_Name        TEXT,
    Cus_Phonenumber INTEGER,
    Cus_Address     TEXT (100) 
);


-- Table: Employee
DROP TABLE IF EXISTS Employee;

CREATE TABLE Employee (
    EM_ID         INTEGER   PRIMARY KEY,
    EM_Firstname  TEXT (50),
    EM_Surname    TEXT (50),
    EM_Position   TEXT (50),
    EM_IP_Address INTEGER,
    EM_Phone      INTEGER,
    EM_Salary     INTEGER
);


-- Table: Order
DROP TABLE IF EXISTS [Order];

CREATE TABLE [Order] (
    Order_ID       INTEGER   PRIMARY KEY,
    Order_Name     TEXT (50),
    Oder_Date      DATE,
    Order_Time     TIME,
    Order_total    INTEGER,
    Order_discount INTEGER,
    Order_cash     INTEGER,
    Order_change   INTEGER,
    Cus_ID         INTEGER   REFERENCES customer (Cus_ID),
    Store_ID       INTEGER   REFERENCES Store (Store_ID),
    EM_ID          INTEGER   REFERENCES Employee (EM_ID) 
);


-- Table: Order_item
DROP TABLE IF EXISTS Order_item;

CREATE TABLE Order_item (
    Order_item_ID     INTEGER   PRIMARY KEY,
    Order_item_Name   TEXT (50),
    Order_item_Amount INTEGER,
    Order_item_Price  INTEGER,
    Order_total       INTEGER   REFERENCES [Order] (Order_total),
    Order_ID          INTEGER   REFERENCES [Order] (Order_ID) 
);


-- Table: Product
DROP TABLE IF EXISTS Product;

CREATE TABLE Product (
    Product_ID         INTEGER   PRIMARY KEY,
    Product_Name       TEXT (50),
    Product_Price      INTEGER,
    Product_sweetLevel INTEGER,
    Product_image,
    Product_size       TEXT,
    Product_type       INTEGER,
    category_id        INTEGER   REFERENCES Category (category_id) 
);


-- Table: Stock
DROP TABLE IF EXISTS Stock;

CREATE TABLE Stock (
    Stock_ID      INTEGER   PRIMARY KEY,
    Stock_Name    TEXT (50),
    Stock_Minimum INTEGER,
    Stock_remain  INTEGER,
    Stock_type    CHAR,
    Stock_Date    DATE
);

INSERT INTO Stock (
                      Stock_ID,
                      Stock_Name,
                      Stock_Minimum,
                      Stock_remain,
                      Stock_type,
                      Stock_Date
                  )
                  VALUES (
                      1,
                      'ผงชาเขียว',
                      1,
                      2,
                      '1',
                      '22/08/2002'
                  );

INSERT INTO Stock (
                      Stock_ID,
                      Stock_Name,
                      Stock_Minimum,
                      Stock_remain,
                      Stock_type,
                      Stock_Date
                  )
                  VALUES (
                      2,
                      'matcha',
                      1,
                      2,
                      '1',
                      '22/09/2002'
                  );


-- Table: Stock Details
DROP TABLE IF EXISTS [Stock Details];

CREATE TABLE [Stock Details] (
    STD_ID       INTEGER PRIMARY KEY,
    Stock_name   TEXT    REFERENCES Stock (Stock_Name),
    STD_quantity INTEGER,
    Stock_remain INTEGER REFERENCES Stock (Stock_remain),
    STD_Date     DATE
);


-- Table: StockM
DROP TABLE IF EXISTS StockM;

CREATE TABLE StockM (
    Stock_name   TEXT    REFERENCES Stock (Stock_Name),
    Stock_remain INTEGER REFERENCES Stock (Stock_remain) 
);


-- Table: Store
DROP TABLE IF EXISTS Store;

CREATE TABLE Store (
    Store_ID      INTEGER    PRIMARY KEY,
    Store_name    TEXT (50),
    Stroe_address TEXT (100),
    Store_tel     INTEGER,
    Store_tax     INTEGER,
    Store_logo
);


-- Table: user
DROP TABLE IF EXISTS user;

CREATE TABLE user (
    user_id       INTEGER   PRIMARY KEY ASC AUTOINCREMENT,
    user_login    TEXT (50),
    user_gender   TEXT (3)  NOT NULL,
    user_password TEXT (50) NOT NULL,
    user_role     INTEGER   NOT NULL,
    user_name     TEXT (50) 
);

INSERT INTO user (
                     user_id,
                     user_login,
                     user_gender,
                     user_password,
                     user_role,
                     user_name
                 )
                 VALUES (
                     1,
                     'user',
                     'm',
                     'password',
                     'dd',
                     'dsd'
                 );

INSERT INTO user (
                     user_id,
                     user_login,
                     user_gender,
                     user_password,
                     user_role,
                     user_name
                 )
                 VALUES (
                     2,
                     'nick',
                     'm',
                     'password',
                     'admin',
                     'satit'
                 );

INSERT INTO user (
                     user_id,
                     user_login,
                     user_gender,
                     user_password,
                     user_role,
                     user_name
                 )
                 VALUES (
                     3,
                     'bb',
                     'M',
                     'bb',
                     0,
                     'bb'
                 );

INSERT INTO user (
                     user_id,
                     user_login,
                     user_gender,
                     user_password,
                     user_role,
                     user_name
                 )
                 VALUES (
                     4,
                     'ng',
                     'M',
                     'nn',
                     0,
                     'nn'
                 );

INSERT INTO user (
                     user_id,
                     user_login,
                     user_gender,
                     user_password,
                     user_role,
                     user_name
                 )
                 VALUES (
                     5,
                     'nink',
                     'F',
                     'vvvvvvvvv',
                     1,
                     'nick'
                 );


COMMIT TRANSACTION;
PRAGMA foreign_keys = on;
