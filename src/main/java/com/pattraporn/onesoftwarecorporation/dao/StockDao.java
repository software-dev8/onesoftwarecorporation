/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pattraporn.onesoftwarecorporation.dao;
import com.pattraporn.onesoftwarecorporation.helper.DatabaseHelper;
import com.pattraporn.onesoftwarecorporation.model.Stock;
import com.pattraporn.onesoftwarecorporation.model.User;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
/**
 *
 * @author Pattrapon N
 */
public class StockDao implements Dao<Stock> {
    @Override
    public Stock get(int id) {
        Stock stock = null;
        String sql = "SELECT * FROM Stock WHERE Stock_ID=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                stock = Stock.fromRS(rs);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return  stock;
    }

    public Stock getByName(String name) {
       Stock stock = null;
        String sql = "SELECT * FROM Stock WHERE Stock_Name=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, name);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                stock = Stock.fromRS(rs);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return stock;
    }

    public List<Stock> getAll(String name) {
        ArrayList<Stock> list = new ArrayList();
        String sql = "SELECT * FROM Stock";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                Stock stock = Stock.fromRS(rs);
                list.add(stock);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }
    
    @Override
    public List<Stock> getAll(String where, String order) {
        ArrayList<Stock> list = new ArrayList();
        String sql = "SELECT * FROM Stock where " + where + " ORDER BY" + order;//ยังไม่แก้
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
               Stock stock = Stock.fromRS(rs);
                list.add(stock);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }
    

  /*  public List<Stock> getAll(String order) {
        ArrayList<Stock> list = new ArrayList();
        String sql = "SELECT * FROM Stock  ORDER BY" + order; //ยังไม่แก้
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
              Stock stock = Stock.fromRS(rs);
                list.add(stock);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }*/

    @Override
    public Stock save(Stock obj) {

        String sql = "INSERT INTO Stock ( Stock_Name, Stock_Minimum, Stock_remain, Stock_type,Stock_Date)"
                + "VALUES(?, ?, ?, ?, ?)";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, obj.getName());
            stmt.setInt(2, obj.getMinimum());
            stmt.setInt(3, obj.getRemain());
            stmt.setString(4, obj.getType());
            stmt.setString(5, obj.getDate());
//            System.out.println(stmt);
            stmt.executeUpdate();
            int id = DatabaseHelper.getInsertedId(stmt);
            obj.setId(id);
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
        return obj;
    }

    @Override
    public Stock update(Stock obj) {
        String sql = "UPDATE Stock"
                + " SET Stock_Name = ?, Stock_Minimum = ?,Stock_remain = ?, Stock_type = ?, Stock_Date= ?"
                + " WHERE Stock_ID = ?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
           stmt.setString(1, obj.getName());
            stmt.setInt(2, obj.getMinimum());
            stmt.setInt(3, obj.getRemain());
            stmt.setString(4, obj.getType());
            stmt.setString(5, obj.getDate());
            stmt.setInt(6, obj.getId());
//            System.out.println(stmt);
            int ret = stmt.executeUpdate();
            System.out.println(ret);
            return obj;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
    }

    @Override
    public int delete(Stock obj) {
        String sql = "DELETE FROM Stock WHERE Stock_ID=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getId());
            int ret = stmt.executeUpdate();
            return ret;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return -1;        
    }

    @Override
    public List<Stock> getAll() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
