/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pattraporn.onesoftwarecorporation.dao;

import com.pattraporn.onesoftwarecorporation.helper.DatabaseHelper;
import com.pattraporn.onesoftwarecorporation.model.Customer;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author roman
 */
public class CustomerDao implements Dao <Customer>{

    @Override
    public Customer get(int id) {
        Customer cus = null;
        String sql = "SELECT * FROM Customer WHERE Cus_ID=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement ctmt = conn.prepareStatement(sql);
            ctmt.setInt(1, id);
            ResultSet rs = ctmt.executeQuery();

            while (rs.next()) {
                cus = Customer.fromRS(rs);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return cus;
    }

    @Override
    public List getAll() {
        ArrayList<Customer> list = new ArrayList();
        String sql = "SELECT * FROM Customer";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement ctmt = conn.createStatement();
            ResultSet rs = ctmt.executeQuery(sql);

            while (rs.next()) {
                Customer customer = Customer.fromRS(rs);
                list.add(customer);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }


    public Customer save(Customer obj) {

        String sql = "INSERT INTO Customer ( Cus_Name, Cus_PhoneNumber, Cus_Address)"
                + "VALUES(?, ?, ?)";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement ctmt = conn.prepareStatement(sql);
            ctmt.setString(1, obj.getName());
            ctmt.setInt(2, obj.getPhonenumber());
            ctmt.setString(3, obj.getAddress());
//            System.out.println(ctmt);
            ctmt.executeUpdate();
            int id = DatabaseHelper.getInsertedId(ctmt);
            obj.setId(id);
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
        return obj;
    }


    public Customer update(Customer obj) {
        String sql = "UPDATE Customer"
                + " SET Cus_Name = ?, Cus_PhoneNumber = ?,Cus_Address = ?"
                + " WHERE Cus_ID = ?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement ctmt = conn.prepareStatement(sql);
            ctmt.setString(1, obj.getName());
            ctmt.setInt(2, obj.getPhonenumber());
            ctmt.setString(3, obj.getAddress());
            ctmt.setInt(4, obj.getId());
//            System.out.println(ctmt);
            int ret = ctmt.executeUpdate();
            System.out.println(ret);
            return obj;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
    }


    public int delete(Customer obj) {
        String sql = "DELETE FROM Cus WHERE Cus_ID=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement ctmt = conn.prepareStatement(sql);
            ctmt.setInt(1, obj.getId());
            int ret = ctmt.executeUpdate();
            return ret;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return -1;
    }

    @Override
    public List getAll(String where, String order) {
        ArrayList<Customer> list = new ArrayList();
        String sql = "SELECT * FROM Customer where " + where + " ORDER BY" + order;//ยังไม่แก้
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement ctmt = conn.createStatement();
            ResultSet rs = ctmt.executeQuery(sql);

            while (rs.next()) {
                Customer customer = Customer.fromRS(rs);
                list.add(customer);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    public List<Customer> getAll(String customer_name_asc) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
