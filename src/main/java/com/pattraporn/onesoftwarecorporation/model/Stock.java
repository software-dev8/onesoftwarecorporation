/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pattraporn.onesoftwarecorporation.model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Pattrapon N
 */
public class Stock {
    private int id;
    private String name;
    private int minimum;
    private int remain;
    private String type;
    private String date;
     public Stock(String name, String type){
        this.name=name;
        this.type=type;
    }
    public Stock(int id,String name,int minimum,int remain  , String type,String date){
        this.id = id;
        this.name=name;
        this.minimum = minimum;
        this.remain= remain;
        this.type=type;
        this.date = date;
    }
     public Stock(String name,int minimum,int remain  , String type,String date){
        this.id = -1;
        this.name=name;
        this.minimum = minimum;
        this.remain= remain;
        this.type=type;
        this.date = date;
    }
     public Stock(){
        this.id = -1;
        this.name="";
        this.minimum = 0;
        this.remain= 0;
        this.type="";
        this.date = "";
    
}

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getMinimum() {
        return minimum;
    }

    public void setMinimum(int minimum) {
        this.minimum = minimum;
    }

    public int getRemain() {
        return remain;
    }

    public void setRemain(int remain) {
        this.remain = remain;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    @Override
    public String toString() {
        return "Stock{" + "id=" + id + ", name=" + name + ", minimum=" + minimum + ", remain=" + remain + ", type=" + type + ", date=" + date + '}';
    }
     public static Stock fromRS(ResultSet rs) throws SQLException {
        Stock st = new Stock();
        try {
            st.setId(rs.getInt("Stock_ID"));          
            st.setName(rs.getString("Stock_Name"));
            st.setMinimum(rs.getInt("Stock_Minimum"));
             st.setRemain(rs.getInt("Stock_remain"));
            st.setType(rs.getString("Stock_type"));
            st.setDate(rs.getString("Stock_Date"));
        } catch (SQLException ex) {
            
            Logger.getLogger(Stock.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return st;
    }
    
}
