/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.pattraporn.onesoftwarecorporation.service;

import com.pattraporn.onesoftwarecorporation.dao.EmployeeDao;

import com.pattraporn.onesoftwarecorporation.model.Employee;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author User
 */
public class EmployeeService {
     private static ArrayList<Employee> Employeelist = new ArrayList<>();

     public static boolean addEmployee(Employee employee) {
       Employeelist.add(employee);
        return true;
    }

    public static boolean addEmployee(String name, int phone) {
        Employeelist.add(new Employee( name,phone));
        return true;
    }

   
   
    
  public List<Employee>getEmployee(){
      EmployeeDao employeeDao = new EmployeeDao();
      return employeeDao.getAll("Employee_name asc");
  }
   public Employee addNew(Employee edtedEmployee) {
        EmployeeDao employeeDao = new EmployeeDao();
         return employeeDao.save(edtedEmployee);
    }

    public Employee update(Employee edtedEmployee) {
          EmployeeDao employeeDao = new EmployeeDao();
         return  employeeDao.update(edtedEmployee);
    }

    public int delete(Employee edtedEmployee) {
       EmployeeDao employeeDao = new EmployeeDao();
         return employeeDao.delete(edtedEmployee);
    }

}
