/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.pattraporn.onesoftwarecorporation.model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author User
 */
public class Employee {
     private int id;
    private String name;
    private String surname;
    private String position;
    private int ip_address;
    private int phone;
    private int salary;
     public Employee(String name, int phone){
        this.name=name;
        this.phone=phone;
    }
    public Employee(int id,String name,String surname,String position  , int ip_address,int phone,int salary){
        this.id = -1;
        this.name=name;
        this.surname = surname;
        this.position= position;
        this.ip_address = ip_address;
        this.phone = phone;
        this.salary=salary;
    }

    public Employee() {
        this.id = -1;
        this.name="";
        this.surname = "";
        this.position= "";
        this.ip_address = 0;
        this.phone = 0;
        this.salary=0; }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public int getIp_address() {
        return ip_address;
    }

    public void setIp_address(int ip_address) {
        this.ip_address = ip_address;
    }

    public int getPhone() {
        return phone;
    }

    public void setPhone(int phone) {
        this.phone = phone;
    }

    public int getSalary() {
        return salary;
    }

    public void setSalary(int salary) {
        this.salary = salary;
    }
 

    @Override
    public String toString() {
        return "Employee{" + "id=" + id + ", name=" + name + ", surname=" + surname + ", position=" + position + ", ip_address=" + ip_address + ", phone=" + phone +", salary=" + salary + '}';
    }
     public static Employee fromRS(ResultSet rs) throws SQLException {
        Employee em = new Employee();
        try {
            em.setId(rs.getInt("EM_ID"));          
            em.setName(rs.getString("EM_Firstname"));
            em.setSurname(rs.getString("EM_Surname"));
             em.setPosition(rs.getString("EM_Position"));
            em.setIp_address(rs.getInt("EM_IP_Address"));
            em.setPhone(rs.getInt("EM_Phone"));
            em.setSalary(rs.getInt("EM_Salary"));
        } catch (SQLException ex) {
            
            Logger.getLogger(Stock.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return em;
    }
    
}

